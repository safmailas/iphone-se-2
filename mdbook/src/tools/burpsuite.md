# Burp Suite

## Summary

[Burp Suite](https://portswigger.net/burp) is a great tool that essentially acts as a middle-man between your client (browser, device, etc.) and a server it is trying to communicate with. You're able to capture, inspect, replay, modify, etc. all the network requests sent between the two.

Unfortunately, it can take a good amount of work to get it to cooperate with iOS.

## Installation

Installation is pretty straight forward, you just go to the [Download](https://portswigger.net/burp/communitydownload) page and install it on your local workstation.

## Configuration

I'm currently on Windows 10, so depending on your OS, some things are going to be a LITTLE different, but I'm hoping that this will provide you a good starting point. I do not believe you need a jailbroken device for the basic functionality of Burp Suite, but it certainly doesn't hurt. I am writing this guide with the following setup:

OS: Windows 10 (10.0.19041 Build 19041)
iDevice: iPhone SE 2
iOS: 13.5 (jailbroken)
Burp Suite: 2020.6A

Huge shout out to the people in [this portswigger thread](https://forum.portswigger.net/thread/ios-13-burp-ssl-certs-not-able-to-be-fully-trusted-2b208cf6) who provided a lot of the missing pieces.

### Delete Old Stuff

If you're here, you've probably tried a lot of things that may cause conflicts with the setup I'm doing here, so I recommend you start from a clean slate. I'd go to `Settings` => `General` => `Profiles & Device Management` and delete any Burp related certs under `Configuration Profile`. If you have anything under `Developer App`, I would recommend leaving it because that's probably AltStore and how you're jailbreaking. Just click the Profile, click `Remove Profile`, and enter your pin.

### Generate a Certificate

Apple decided to make our lives hard by [hardening the requirements for trusted certs in iOS 13](https://support.apple.com/en-us/HT210176) so we will need to create our own certs rather than using the ones Burp Suite generates.

I have Ubuntu 20.04 installed with WSL2 on Windows 10 and I don't want to go into how to set that up here, but essentially, you just need access to the `openssl` command.

Run the following commands to generate your new certificates:

> Create Certificate Directory
>
>> ```
>> cd /mnt/c/Program\ Files/BurpSuite*
>> mkdir certificates
>> cd certificates
>>```
> 
> Generate Certificates
>
>> ```
>> openssl req -x509 -nodes -newkey rsa:4096 -keyout myBurpCA.key -out root-ca.crt -days 365 -subj "/C=CA/O=Burp/OU=Certification Services/CN=MyBURPRootCA/" -addext "extendedKeyUsage=1.3.6.1.5.5.7.3.1"
>> openssl pkcs12 -export -out BurpRootCA.pfx -inkey myBurpCA.key -in root-ca.crt
>> ```

You will be asked to enter a password. Make sure it is one you will remember.

### Configure Burp Suite to use TLS 1.2

It would appear that you need to make Burp Suite use TLS 1.2 in order to fully get it to work as TLS 1.3 will cause problems. You can do this by editing `C:\Program Files\BurpSuitePro\BurpSuitePro.vmoptions`.

> Before
>
>> ```
>> # Enter one VM parameter per line
>> # For example, to adjust the maximum memory usage to 512 MB, uncomment the following line:
>> # -Xmx512m
>> # To include another file, uncomment the following line:
>> # -include-options [path to other .vmoption file]
>> 
>> -XX:MaxRAMPercentage=50
>> ```
>
> After
>
>> ```
>> # Enter one VM parameter per line
>> # For example, to adjust the maximum memory usage to 512 MB, uncomment the following line:
>> # -Xmx512m
>> # To include another file, uncomment the following line:
>> # -include-options [path to other .vmoption file]
>> 
>> -XX:MaxRAMPercentage=50
>> -Dhttps.protocols=TLSv1,TLSv1.1,TLSv1.2
>> -Djdk.tls.server.protocols=TLSv1,TLSv1.1,TLSv1.2
>> ```

### Import Certificate into Burp Suite

Open Burp Suite and navigate to `Proxy` => `Options` and select `Import/export CA certificate`.

Under `Import`, select `Certificate and private key from PKCS#12 keystore`. Click `Select file...` and navigate to the folder you created the certificates in. Select the file ending with `.pfx` and enter your password. Click Next and Close.

While you're here, make sure you select the `Proxy Listener`, click `Edit`, and change the address to either `All interfaces` or the external IP of your computer on the same network as your iDevice.

### Install Certificates on iPhone

On your iDevice, go to `Settings` => `Wi-Fi` and select the blue `i` next to your network name. Select `Configure Proxy` under `HTTP Proxy` and select `Manual. Enter the Burp IP Address and Port and click `Save`.

Open `Safari` and navigate to `http://burp`. Click `CA Certificate` in the top right corner and click `Allow`. It will tell your that the `Profile Downloaded` and that you will need to install it in the `Settings` app.

Open `Settings` and you should see `Profile Downloaded` at the top of the main Settings page. Click it. If you do not see it, go to `General` => `Profiles & Device Management` and you should see it under `Downloaded Profile`. Click it and click `Install`. Enter your passcode and click `Install` two more times. You should now see a green `Verified` checkmark.

Now you need to tell the iPhone that it should absolutely trust this certificate authority. Navigate to `Settings` => `General` => `About` => `Certificate Trust Settings` and flip the grey selector to green next to the name of your certificate authority.

## Smoke Test

The easiest way to test that this is working properly is to go to `https://burp`. You should see a lock next to `burp` in the address bar. That's boring, though. You should now be able to do whatever you like, such as snooping on nearly all programs and visiting any website.

Supposedly there may be some issues with certain apps, and I don't really know the exact implications of disabling TLS 1.3 since most websites do not strictly require only TLS 1.3, but keep in mind that we did disable it in case you run into issues in the future.

If you're still running into issues, I DO have SSL Kill Switch 2 installed as described on my page for that, so I'd give that a shot as well. I don't actually know how much it is affecting the fact that I am able to snoop on any app I've tried.
