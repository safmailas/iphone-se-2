# Frida

## Summary

[Frida](https://frida.re/) is an awesome utility that has made its name known across Mobile Penetration Testers. It allows you to write JavaScript code to inject into running processes in order to change the function of processes and methods/functions on the fly. As an example, if you're testing an application and would like to bypass a login request, you can use Frida to hook the function checking if you supplied the proper password to return that you did.

## Installation

1. Start `Cydia` and navigate to the `Sources` Page.
1. Click `Edit` in the top right corner, then `Add` in the top left.
1. Enter `https://build.frida.re` aand click `Add Source`.
1. Click on `build.frida.re` in your list of sources and click `All Packages`.
1. Install the corresponding package for your device.
1. Reboot

I chose `Frida for A12+ devices on my `iPhone SE 2`. If you don't know what your device is, you can check the following list:

- Frida for 32-bit devices:
  - Devices released on/before September 2012 (iPhone 5 and older)
- Frida for pre-A12 devices:
  - Devices released between September 2013 and September 2017 (iPhone 5S to iPhone 8/X)
- Frida for A12+ devices:
  - Devices released after September 2018 (iPhone XS/XR and newer)

## Smoke Test

I won't go through how to install Frida on your workstation, but it needs to be done. You can find instructions [here](https://frida.re/docs/installation/).

If you haven't done so already, you will need to make sure that the device is connected via USB and unlocked. Then run the following command:

```
idevicepair pair
```

You will be prompted with `ERROR: Please accept the trust dialog on the screen of device <UDID>, then attempt to pair again.` Click the `Trust` button on the dialog on your iPhone and run the previous command again. You should now see `SUCCESS: Paired with device <UDID>`

You can test that everything is working by running the following command:

```
frida-ps -U
```

If you recieve the message `Failed to enumerate processes: this feature requires an iOS Developer Disk Image to be mounted; run Xcode briefly or use ideviceimagemounter to mount one manually`, you will need to download the Developer Disk Image from [this GitHub Repository](https://github.com/xushuduo/Xcode-iOS-Developer-Disk-Image/releases). It needs to match the iOS version on your device, so since I'm usingiOS 13.5, I would download [this](https://github.com/xushuduo/Xcode-iOS-Developer-Disk-Image/releases/download/13.5/13.5.17F61.zip) Disk Image.

Once downloaded, unzip it and go to its directory. Unlock the device and run the following command:

```
ideviceimagemounter DeveloperDiskImage.dmg DeveloperDiskimage.dmg.signature
```

If you're getting `mount_image returned -3`, you may be fine. Try `frida-ps -U` again. I spent way too long trying to figure out why it wasn't working, when it really was.

You should see output similar to:

```
PID  Name
---  --------------------------------------------------------
569  Cydia
957  Settings
546  Siri Search
451  ACCHWComponentAuthService
439  AppleCredentialManagerDaemon
561  AssetCacheLocatorService
472  BlueTool
518  CAReportingService
552  CMFSyncAgent
494  CloudKeychainProxy
448  CommCenter
463  CommCenterMobileHelper
555  ContainerMetadataExtractor
...
```

## Usage

I may add to this with some of my own stuff at some point, but for now, I recommend you check out the [OWASP MSTG Crackme's](https://github.com/OWASP/owasp-mstg/tree/master/Crackmes) as several of those have iOS Frida solutions.
