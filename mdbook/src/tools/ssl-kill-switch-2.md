# SSL Kill Switch 2

## Summary

[SSL Kill Switch 2](https://github.com/nabla-c0d3/ssl-kill-switch2) can be installed as a Cydia Substrate tweak on a jailbroken device and allows you to perform a man-in-the-middle attack on anything running on your iPhone, regardless if the developer of whatever application properly pinned their SSL certificates. This makes the device incredible insecure, but it great when we want to snoop traffic using something like Burp Suite.

## Installation

I had a lot of trouble getting Burp Suite to work with iOS 13.5, but one of the things I have installed in my current (working) configuration is a modified SSL Kill Switch 2 from julioverne that was a result of a [TweakBounty](https://www.reddit.com/r/TweakBounty/comments/gw1ems/40135_ssl_kill_switch_2_update/).

Open `Cydia` and go to `Sources` => `Edit` => `Add` and add the following URL:

```
https://julioverne.github.io
```

Navigate to `julioverne's Repo` => `Tweaks` => `SSL Kill Switch 2 (iOS 13)` and Install it.

Open `Settings` => `SSL Kill Switch 2` and toggle `Disable Certificate Validation` to on (green).
