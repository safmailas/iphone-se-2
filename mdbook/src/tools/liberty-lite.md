# Liberty Lite

## Summary

NOTE: This isn't working for me on iOS 13.5 on the iPhone SE 2 as of June 4th, 2020. I'm going to leave it installed in hopes that it eventually works, though.

Liberty Lite is a tweak that can be installed via Cydia to hide the fact that you are on a jailbroken device. In order to properly verify that Liberty Lite is working, I recommend having an app in mind that you know will not run if a Jailbreak is detected. Try to open it before and after the following steps.

## Installation

1. Start `Cydia` and navigate to the `Sources` page.
1. Click `Edit` in the top right corner, then `Add` in the top left.
1. Enter `https://ryleyangus.com/repo` and click `Add Source`.
1. Click on `Ryley's Repo` in your list of sources and click `All Packages`.
1. I saw several reports that `Liberty Lite (Beta)` is the only one to work with iOS 13, so I installed that one.
1. Click `Restart Springboard`.

## Configuration

Go to iOS `Settings` => `Liberty Lite` => `Block Jailbreak Detection` and enable the applications you wish to hide your jailbreak on.
