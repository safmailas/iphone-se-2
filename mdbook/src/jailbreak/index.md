# Jailbreak

## Summary

First, keep in mind that jailbreaking your device may damage your device if you are not careful and it may interfere with your warranty. I take no responsibility if something bad happens and you have to buy a new device.

I received my iPhone SE 2 (2020) on June 4th, 2020 and needed to jailbreak to best leverage the iPhone's capabilities to assist in iOS Pen Testing.

unc0ver 5.0.1 was release on May 25th, 2020 and exploited a Kernel bug in iOS 13.5, which allowed every single iOS device to have a semi-untethered jailbreak. Semi-untethered means it will persist until you reboot your iPhone, but you are able to jailbreak again without the aid of any other devices. 

Apple patched this Kernel bug with 13.5.1 on June 1st, 2020. As of June 4th, 2020, Apple is still signing iOS 13.5, which means that you can still upgrade to this version if you're on a lower version. Apple will, however, stop signing 13.5 very soon to stop people from jailbreaking, so if you have the ability to get on 13.5, I highly recommend doing so and dumping your .SHSH2 blob so that if you decide to keep your device Jailbroken on 13.5 and something bad happens, you can easily restore to 13.5 well after Apple stops signing iOS 13.5 and prohibits people from installing it via official means.

