# unc0ver 5.0.1

## Summary

In this phase, we will be jailbreaking our iOS 13.5 device with unc0ver 5.0.1. I did this on Windows using AltStore, but there are instructions on [the unc0ver website](https://unc0ver.dev) on how to do this on iOS, macOS, Windows, and Linux. Some methods cost money ($100/yr for an Apple Developer account) or require you to already be jailbroken. This method requires neither.

## Required Software

You will need to download and install the following software:

- iTunes (NOT UWP version. If you installed from the Microsoft Store, uninstall it first)
  - [64-Bit](https://www.apple.com/itunes/download/win64)
  - [32-Bit](https://www.apple.com/itunes/download/win32)
- [iCloud](https://secure-appldnld.apple.com/windows/061-91601-20200323-974a39d0-41fc-4761-b571-318b7d9205ed/iCloudSetup.exe)
- [AltStore](https://www.altstore.io/)

I won't walk through the installation since it's pretty standard affair. Install them all and make sure they run. To confirm AltStore has properly been installed, check for it (a diamond icon) in your Windows taskbar. You will need to run it first before it will appear here.

## Install AltStore

Open iTunes and make sure your iDevice is connected.

![iTunes iOS 13.5](../images/itunes-13.5.png)

Click on the AltStore diamond in the Windows taskbar, and click `Install AltStore` => `<iDevice Name>`.

![Install AltStore](../images/install-altstore.png)

Enter your Apple ID username and password.

![AltStore Apple ID](../images/altstore-apple-id.png)

You might be prompted to install iCloud if it isn't already installed.

![AltStore iCloud Download](../images/altstore-icloud-download.png)

You will then be prompted to enter your 2FA pin.

![AltStore MFA Pin](../images/altstore-mfa-pin.png)

From here, you should have the AltStore icon on your iPhone.

![AltStore Installed](../images/altstore-installed.png)

## Install unc0ver 5.0.1

In Safari on your iDevice, navigate to [https://unc0ver.dev](https://unc0ver.dev) and click `Download v5.0.1`. Save the `unc0ver-v5.0.1.ipa` to your iDevice and open AltStore.

![unc0ver on Safari](../images/unc0ver-safari.png)

Click the `My Apps` option at the bottom and the `+` icon in the top left corner. Select `unc0ver-v5.0.1.ipa` and install it.

![AltStore - My Apps](../images/altstore-my-apps.png)
![AltStore - Files - unc0ver](../images/altstore-files-unc0ver.png)

You will now see its icon on your iPhone, however, if you try to run it, it will say it was created by an `Untrusted Developer`.

![unc0ver - Untrusted Developer](../images/unc0ver-untrusted-developer.png)

Go to `Settings` => `General` => `Device Management` => `<your email>` and click `Trust "<your email>"`. Click `Trust` on the prompt. It should now open properly.

![Developer Trust Page](../images/developer-trust-page.png)
![Developer Trust Popup](../images/developer-trust-popup.png)

## Jailbreak

Open the `unc0ver` app and click `Jailbreak`.

![unc0ver - First Run](../images/unc0ver-first-run.png)

It will prompt you that you will need to reboot to finish the jailbreak process. Click `OK`.

![unc0ver - First Result](../images/unc0ver-first-result.png)

Open the `unc0ver` app and click `Jailbreak` again.

It will prompt you that `No error occurred` and that the device will `reboot into the jailbroken state`.

![unc0ver - Second Result](../images/unc0ver-second-result.png)

## Cydia

One of the hallmarks of a Jailbroken iDevice is the Cydia application. This should now be present on your homescreen.

![Cydia on Home](../images/cydia-on-home.png)

## Prohibit Future Updates

I highly recommend that you stop your iPhone from updating if you wish to say on the jailbreakable 13.5. There is a bug in unc0ver 5.0.1 that makes this a little counter-intuitive. If you open iOS `Settings` => `General` => `Software Update`, you will see that 13.5.1 is available. This should not happen.

![13.5.1 Update Available](../images/13.5.1-update-available.png)

First, let's stop it the "official" way and click on `Automatic Updates` and set the toggle to `Off`.

![Disable Automatic Updates](../images/disable-automatic-updates.png)

Now let's do it the unc0ver way for additional protection. Launch the `unc0ver` app and select the cog in the top left corner. Note that the `Disable Auto Updates` toggle is currently `Blue` (`On`). You must turn this to `Black` (`Off`) in order for updates to be stopped. I know this is backwards. This is the bug I mentioned earlier. Click `Done` and click `Re-Jailbreak`.

![unc0ver - Disable Updates](../images/unc0ver-disable-updates.png)
![unc0ver - Re-Jailbreak](../images/unc0ver-re-jailbreak.png)

Now when you go to iOS `Settings` => `General` => `Software Update`, you will see that it was `Unable to Check for Update`. This is the desired state and you are now safe.

![Unable to Update](../images/unable-to-update.png)

